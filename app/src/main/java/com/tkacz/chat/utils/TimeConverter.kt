package com.tkacz.chat.utils

import java.text.SimpleDateFormat
import java.util.*

interface TimeConverter {

    companion object {
        const val DATE_PATTERN = "yyyy-MM-dd HH:mm"
    }

    fun convertTimestampToDate(timestamp: Long): String {
        val simpleDate = SimpleDateFormat(DATE_PATTERN, Locale.ENGLISH)
        return simpleDate.format(timestamp)
    }
}