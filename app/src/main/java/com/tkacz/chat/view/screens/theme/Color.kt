package com.tkacz.chat.view.screens.theme

import androidx.compose.ui.graphics.Color

val PrimaryColor = Color(0xff7070db)
val TextColor = Color(0xFF000000)
val BackgroundColor = Color(0xFFFFE8F0)
val ErrorColor = Color(0xFFFF0000)
val TextFieldColor = Color(0xFF0277bd)
val ButtonColor = Color(0xFF0277bd)
