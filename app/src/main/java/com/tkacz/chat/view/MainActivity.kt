package com.tkacz.chat.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.tkacz.chat.MyApplication
import com.tkacz.chat.databinding.ActivityMainBinding
import com.tkacz.chat.di.AppComponent
import com.tkacz.chat.view.fragments.MainFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    // Stores an instance of RegistrationComponent so that its Fragments can access it
    lateinit var appComponent: AppComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        appComponent = (application as MyApplication).appComponent
        appComponent.inject(this)

        MainFragment.show(supportFragmentManager)
    }
}