package com.tkacz.chat.view.screens

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.tkacz.chat.data.models.Message
import com.tkacz.chat.view.screens.theme.BackgroundColor
import com.tkacz.chat.view.screens.theme.TextFieldColor

@Composable
fun MessagesScreen(items: List<Message>, image: String, convertTimestamp: (Long) -> String) {
    Surface(color = BackgroundColor, modifier = Modifier.fillMaxSize()) {
        LazyColumn(
            modifier = Modifier.fillMaxSize()
        ) {
            items(items.size) { i ->
                MessageCard(items[i], image, convertTimestamp)
            }
        }
    }
}

@Composable
fun MessageCard(msg: Message, image: String, convertTimestamp: (Long) -> String) {

    Row(modifier = Modifier.padding(all = 8.dp)) {
        AsyncImage(
            model = image,
            contentDescription = "",
            Modifier
                .padding(5.dp)
                .size(50.dp)
                .clip(CircleShape)
        )

        // Add a horizontal space between the image and the column
        Spacer(modifier = Modifier.width(8.dp))

        Column {
            Text(text = msg.author, color = TextFieldColor)
            // Add a vertical space between the author and message texts
            Spacer(modifier = Modifier.height(4.dp))
            Text(text = msg.text)
            Spacer(modifier = Modifier.height(4.dp))
            Text(text = convertTimestamp.invoke(msg.timestamp))
        }
    }
}