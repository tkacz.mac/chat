package com.tkacz.chat.view.states

enum class SearchFieldState {
    Idle, Opened, Closed
}