package com.tkacz.chat.view

import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import com.tkacz.chat.MyApplication
import com.tkacz.chat.utils.Constants
import com.tkacz.chat.view.screens.LoginScreen
import com.tkacz.chat.view.screens.theme.ChatAppScreensTheme
import com.tkacz.chat.viemodels.LoginViewModel
import javax.inject.Inject

class LoginActivity : AppCompatActivity() {

    @Inject
    lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (application as MyApplication).appComponent.inject(this)

        setContent {
            val emailMsg = resources.getString(loginViewModel.emailState.value.getMessage())
            val passwordMsg = resources.getString(loginViewModel.passwordState.value.getMessage())

            ChatAppScreensTheme {

                LoginScreen(
                    emailMsg,
                    passwordMsg,
                    validate = { email, pass ->
                        loginViewModel.updateIngredientStates(email, pass)
                    },
                    startActivity = {
                        if (!loginViewModel.canMoveToAnotherActivity() && !Constants.skipLoginActivity) return@LoginScreen
                        val intent = Intent(this@LoginActivity, MainActivity::class.java)
                        startActivity(intent)
                    }
                )
            }
        }
    }
}