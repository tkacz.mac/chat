package com.tkacz.chat.view.states

import androidx.compose.runtime.MutableState

interface InputTextStateLogic {

    fun processInputTextLogic(
        value: String,
        state: MutableState<InputTextState>,
        validator: (String) -> Boolean
    ) {
        state.value = when {
            value.isEmpty() -> InputTextState.Empty
            validator(value) -> InputTextState.Correct
            else -> InputTextState.Incorrect
        }
    }

    fun inputTextStatesAreCorrect(
        firstState: InputTextState,
        secondState: InputTextState
    ): Boolean {
        return firstState == InputTextState.Correct && secondState == InputTextState.Correct
    }
}