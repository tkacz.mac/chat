package com.tkacz.chat.view.screens

import androidx.compose.material.Scaffold
import androidx.compose.material.ScaffoldState
import androidx.compose.runtime.Composable
import com.tkacz.chat.data.models.ChatItem
import com.tkacz.chat.view.models.MenuItem
import com.tkacz.chat.view.screens.componets.AppBar
import com.tkacz.chat.view.screens.componets.DrawerBody
import com.tkacz.chat.viemodels.SearchViewModel

@Composable
fun MainScreen(
    searchViewModel: SearchViewModel,
    scaffoldState: ScaffoldState,
    drawerItems: List<MenuItem>,
    chatsItems: List<ChatItem>,
    onNavigationIconClick: () -> Unit,
    convertTimestamp: (Long) -> String,
    openMessagesFragment: (String, String) -> Unit
) {
    Scaffold(
        scaffoldState = scaffoldState,
        drawerGesturesEnabled = scaffoldState.drawerState.isOpen,
        topBar = {
            AppBar(searchViewModel, onNavigationIconClick)
        },
        drawerContent = { DrawerBody(items = drawerItems) }
    ) {
        ChatsScreen(chatsItems, it, convertTimestamp, openMessagesFragment)
    }
}