package com.tkacz.chat.view.screens

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import com.tkacz.chat.view.screens.theme.*

@Composable
fun LoginScreen(
    emailMsg: String,
    passwordMsg: String,
    validate: (String, String) -> Unit,
    startActivity: () -> Unit
) {
    var email by remember { mutableStateOf("") }
    var pass by remember { mutableStateOf("") }

    Surface(color = BackgroundColor, modifier = Modifier.fillMaxSize()) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {

            SetTextField(
                "Email Address",
                setValue = { email = it },
                getValue = { email },
                KeyboardType.Email,
                false
            )
            Text(text = emailMsg, style = TextStyle(color = ErrorColor))

            SetTextField(
                "Password",
                setValue = { pass = it },
                getValue = { pass },
                KeyboardType.Password,
                true
            )
            Text(text = passwordMsg, style = TextStyle(color = ErrorColor))

            SetButton(click = {
                validate.invoke(email, pass)
                startActivity.invoke()
            })
        }
    }
}

@Composable
fun SetTextField(
    text: String,
    setValue: (String) -> Unit,
    getValue: () -> String,
    keyboardType: KeyboardType,
    passVisualisation: Boolean
) {

    TextField(
        value = getValue.invoke(),
        onValueChange = {
            setValue.invoke(it)
        },
        label = {
            Text(text = text, color = TextFieldColor)
        },
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp)
            .padding(top = 10.dp),
        colors = TextFieldDefaults.outlinedTextFieldColors(
            unfocusedBorderColor = TextFieldColor,
            textColor = TextColor
        ),
        visualTransformation = if (passVisualisation) PasswordVisualTransformation() else VisualTransformation.None,
        keyboardOptions = KeyboardOptions(
            keyboardType = keyboardType
        ),
        singleLine = true,
    )
}

@Composable
fun SetButton(click: () -> Unit) {
    Button(
        onClick = {
            click.invoke()
        },
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp)
            .padding(top = 20.dp),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = ButtonColor,
            contentColor = Color.White
        ),
        contentPadding = PaddingValues(vertical = 14.dp)
    ) {
        Text(text = "Login")
    }
}