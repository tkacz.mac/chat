package com.tkacz.chat.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.FragmentManager
import com.tkacz.chat.R
import com.tkacz.chat.utils.TimeConverter
import com.tkacz.chat.viemodels.ChatsViewModel
import com.tkacz.chat.viemodels.SearchViewModel
import com.tkacz.chat.view.MainActivity
import com.tkacz.chat.view.models.MenuItemsLoader
import com.tkacz.chat.view.screens.MainScreen
import com.tkacz.chat.view.screens.theme.ChatAppScreensTheme
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainFragment : Fragment(), MenuItemsLoader, TimeConverter {

    @Inject
    lateinit var searchViewModel: SearchViewModel

    @Inject
    lateinit var chatsViewModel: ChatsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (activity as MainActivity).appComponent.inject(this)
        chatsViewModel.updateChatItems()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                ChatAppScreensTheme {
                    val scaffoldState = rememberScaffoldState()
                    val scope = rememberCoroutineScope()
                    val chatItems = chatsViewModel.chatItems.value

                    MainScreen(
                        searchViewModel,
                        scaffoldState,
                        loadMenuItems(),
                        chatItems,
                        onNavigationIconClick = {
                            scope.launch { scaffoldState.drawerState.open() }
                        },
                        convertTimestamp = {
                            convertTimestampToDate(it)
                        },
                        openMessagesFragment = { author, image ->
                            MessagesFragment.show(
                                activity?.supportFragmentManager ?: return@MainScreen, author, image
                            )
                        }
                    )
                }
            }
        }
    }

    companion object {
        fun show(fragmentManager: FragmentManager) {
            val fragment = MainFragment()
            fragmentManager
                .beginTransaction()
                .replace(R.id.fragmentContainerView, fragment)
                .commit()
        }
    }
}