package com.tkacz.chat.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.FragmentManager
import com.tkacz.chat.R
import com.tkacz.chat.utils.TimeConverter
import com.tkacz.chat.viemodels.ChatsViewModel
import com.tkacz.chat.view.MainActivity
import com.tkacz.chat.view.screens.MessagesScreen
import javax.inject.Inject

class MessagesFragment : Fragment(), TimeConverter {

    @Inject
    lateinit var chatsViewModel: ChatsViewModel
    private var user: String? = null
    private var image: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            user = it.getString(ARG_USER)
            image = it.getString(ARG_IMAGE)
        }

        (activity as MainActivity).appComponent.inject(this)

        chatsViewModel.updateMessages(user ?: "")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                val messages = chatsViewModel.messages.value

                MessagesScreen(messages, image ?: "") {
                    convertTimestampToDate(it)
                }
            }
        }
    }

    companion object {

        private const val ARG_USER = "user"
        private const val ARG_IMAGE = "image"

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MessagesFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_USER, param1)
                    putString(ARG_IMAGE, param2)
                }
            }

        fun show(fragmentManager: FragmentManager, peer: String, image: String) {
            val fragment = newInstance(peer, image)
            fragmentManager
                .beginTransaction()
                .replace(R.id.fragmentContainerView, fragment)
                .addToBackStack(null)
                .commit()
        }
    }
}