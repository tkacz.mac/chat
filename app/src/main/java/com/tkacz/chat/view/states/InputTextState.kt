package com.tkacz.chat.view.states

import com.tkacz.chat.R

enum class InputTextState {
    Idle, Empty, Incorrect, Correct;

    fun getMessage(): Int {
        return when (this) {
            Empty -> {
                R.string.empty_value
            }
            Incorrect -> {
                R.string.incorrect_value
            }
            Idle, Correct -> {
                R.string.empty_string
            }
        }
    }
}