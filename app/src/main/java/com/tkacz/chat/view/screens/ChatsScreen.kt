package com.tkacz.chat.view.screens

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.tkacz.chat.data.models.ChatItem
import com.tkacz.chat.view.screens.theme.TextFieldColor

@Composable
fun ChatsScreen(
    items: List<ChatItem>,
    paddingValues: PaddingValues,
    convertTimestamp: (Long) -> String,
    openMessagesFragment: (String, String) -> Unit
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .padding(paddingValues)
    ) {
        items(items.size) { i ->
            ChatCard(items[i], convertTimestamp, openMessagesFragment)
        }
    }
}

@Composable
fun ChatCard(
    item: ChatItem,
    convertTimestamp: (Long) -> String,
    openMessagesFragment: (String, String) -> Unit
) {
    Card(
        elevation = 10.dp,
        border = BorderStroke(1.dp, Color.Gray),
        modifier = Modifier
            .fillMaxSize()
            .clickable {
                openMessagesFragment.invoke(item.peerName, item.image)
            }
            .padding(16.dp),
    ) {
        Row(Modifier.fillMaxSize()) {
            AsyncImage(
                model = item.image,
                contentDescription = "",
                Modifier
                    .padding(5.dp)
                    .size(100.dp)
                    .clip(CircleShape)
            )

            Spacer(modifier = Modifier.width(10.dp))

            Column {
                Text(
                    text = item.peerName,
                    fontSize = 20.sp,
                    color = TextFieldColor
                )
                Text(text = item.lastMsg)
                Text(text = convertTimestamp.invoke(item.timestamp))
            }
        }
    }
}