package com.tkacz.chat.view.models

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExitToApp
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Settings

interface MenuItemsLoader {

    fun loadMenuItems(): List<MenuItem> {
        return listOf(
            MenuItem(
                id = "home",
                title = "Home",
                contentDescription = "Go to home screen",
                icon = Icons.Default.Home
            ),
            MenuItem(
                id = "settings",
                title = "Settings",
                contentDescription = "Go to settings screen",
                icon = Icons.Default.Settings
            ),
            MenuItem(
                id = "help",
                title = "Help",
                contentDescription = "Get help",
                icon = Icons.Default.Info
            ),
            MenuItem(
                id = "log out",
                title = "Log out",
                contentDescription = "Log out to login activity",
                icon = Icons.Default.ExitToApp
            )
        )
    }
}