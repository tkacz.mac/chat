package com.tkacz.chat.data.datasources

import com.tkacz.chat.data.models.Message

interface MessagesLoader {
    fun loadMessages(): List<Message> {
        return listOf(
            Message(
                text = "hello",
                author = "Jessica",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            ),
            Message(
                text = "hello hello",
                author = "Rob",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            ),
            Message(
                text = "hello hello hello",
                author = "Monica",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            ),
            Message(
                text = "hi",
                author = "Rob",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            ),
            Message(
                text = "hi hi",
                author = "Jessica",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            ),
            Message(
                text = "hi hi hi",
                author = "Rob",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            ),
            Message(
                text = "wow",
                author = "Jessica",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            ),
            Message(
                text = "wow wow",
                author = "Monica",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            ),
            Message(
                text = "wow wow",
                author = "Rob",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            ),
            Message(
                text = "hello",
                author = "Jessica",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            ),
            Message(
                text = "hello hello",
                author = "Rob",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            ),
            Message(
                text = "hello hello hello",
                author = "Alice",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            ),
            Message(
                text = "hi",
                author = "Bob",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            ),
            Message(
                text = "hi hi",
                author = "Frank",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            ),
            Message(
                text = "hi hi hi",
                author = "Bob",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            ),
            Message(
                text = "wow",
                author = "Alice",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            ),
            Message(
                text = "wow wow",
                author = "Frank",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            ),
            Message(
                text = "wow wow",
                author = "Bob",
                peer = "tkacz.mac",
                timestamp = 1678649203000
            )
        )
    }
}