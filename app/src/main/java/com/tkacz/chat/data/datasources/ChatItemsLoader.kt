package com.tkacz.chat.data.datasources

import com.tkacz.chat.data.models.ChatItem

interface ChatItemsLoader {

    fun loadChatItems(): List<ChatItem> {
        return listOf(
            ChatItem(
                peerName = "Jessica",
                lastMsg = "hello",
                image = "https://img.freepik.com/free-photo/beautiful-face-young-woman-with-clean-fresh-skin-isolated-white_186202-7897.jpg",
                timestamp = 1678608780000
            ),
            ChatItem(
                peerName = "Rob",
                lastMsg = "You look fantastic today",
                image = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ8IF0KZdsew-IRIm_5rcdUEiQhuBejAuM_Rg&usqp=CAU",
                timestamp = 1678612483000
            ),
            ChatItem(
                peerName = "Monica",
                lastMsg = "Nice to meet you",
                image = "https://images.unsplash.com/photo-1597223557154-721c1cecc4b0?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTV8fGZhY2V8ZW58MHx8MHx8&w=1000&q=80",
                timestamp = 1678612917000
            ),
            ChatItem(
                peerName = "Frank",
                lastMsg = "Nice to meet you",
                image = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS12GZx_dysXJwMG8YSmU8UdKGrsY-HkThFgZyxViCcDOuANclUfXHqAw58s1LzrdbcJzI&usqp=CAU",
                timestamp = 1678612917000
            ),
            ChatItem(
                peerName = "Bob",
                lastMsg = "Buy my car",
                image = "https://media.istockphoto.com/id/1288538088/photo/portrait-young-confident-smart-asian-businessman-look-at-camera-and-smile.jpg?b=1&s=170667a&w=0&k=20&c=EcjlfC0hE33usx5Ys_ftE1iC0TlgKG1pSqclpOULGLk=",
                timestamp = 1678608780000
            ),
            ChatItem(
                peerName = "Alice",
                lastMsg = "You look fantastic today",
                image = "https://media.istockphoto.com/id/464736044/photo/portrait-of-candid-pretty-businesswoman-with-authentic-look.jpg?s=612x612&w=0&k=20&c=mLj9pmi6l8kAahEfbjMBU524zyXpuTwHNexQjUpvvGA=",
                timestamp = 1678612483000
            )
        )
    }
}