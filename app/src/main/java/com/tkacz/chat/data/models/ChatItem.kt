package com.tkacz.chat.data.models

data class ChatItem(
    val peerName: String,
    val lastMsg: String,
    val image: String,
    val timestamp: Long
)
