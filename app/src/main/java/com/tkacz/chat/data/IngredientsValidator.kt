package com.tkacz.chat.data

import javax.inject.Inject

class IngredientsValidator @Inject constructor() {

    companion object {
        const val EMAIL_REGEX = "^[A-Za-z](.*)([@])(.+)(\\.)(.+)"
        const val PASS_REGEX = """^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\S+$).{4,}"""
    }

    fun validateEmail(email: String): Boolean {
        return EMAIL_REGEX.toRegex().matches(email)
    }

    fun validatePassword(password: String): Boolean {
        return PASS_REGEX.toRegex().matches(password)
    }
}