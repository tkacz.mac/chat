package com.tkacz.chat.data.repositories

import com.tkacz.chat.data.datasources.ChatItemsLoader
import com.tkacz.chat.data.datasources.MessagesLoader
import com.tkacz.chat.data.models.ChatItem
import com.tkacz.chat.data.models.Message
import javax.inject.Inject

class ChatsRepository @Inject constructor() : ChatItemsLoader, MessagesLoader {

    fun getChatItemsList(): List<ChatItem> {
        return loadChatItems()
    }

    fun getMessagesRelatedWithAuthor(author: String): List<Message> {
        return loadMessages().filter { it.author == author}
    }
}