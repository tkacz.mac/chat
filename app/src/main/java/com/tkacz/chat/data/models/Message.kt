package com.tkacz.chat.data.models

data class Message(val text: String, val author:String, val peer: String, val timestamp: Long)
