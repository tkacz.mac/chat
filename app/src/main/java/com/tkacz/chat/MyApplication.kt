package com.tkacz.chat

import android.app.Application
import com.tkacz.chat.di.AppComponent
import com.tkacz.chat.di.DaggerAppComponent

class MyApplication : Application() {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.factory().create(applicationContext)
    }
}