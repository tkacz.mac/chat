package com.tkacz.chat.di

import android.content.Context
import com.tkacz.chat.view.LoginActivity
import com.tkacz.chat.view.MainActivity
import com.tkacz.chat.view.fragments.MainFragment
import com.tkacz.chat.view.fragments.MessagesFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component
interface AppComponent {

    // Factory to create instances of the AppComponent
    @Component.Factory
    interface Factory {
        // With @BindsInstance, the Context passed in will be available in the graph
        fun create(@BindsInstance context: Context): AppComponent
    }

    fun inject(activity: LoginActivity)
    fun inject(activity: MainActivity)
    fun inject(fragment: MessagesFragment)
    fun inject(fragment: MainFragment)
}
