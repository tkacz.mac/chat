package com.tkacz.chat.viemodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.tkacz.chat.data.models.ChatItem
import com.tkacz.chat.data.models.Message
import com.tkacz.chat.data.repositories.ChatsRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ChatsViewModel @Inject constructor(private val chatsRepository: ChatsRepository) {

    var chatItems: MutableState<List<ChatItem>> = mutableStateOf(listOf())
    var messages: MutableState<List<Message>> = mutableStateOf(listOf())

    fun updateChatItems() {
        chatItems.value = chatsRepository.getChatItemsList()
    }

    fun updateMessages(author: String) {
        messages.value = chatsRepository.getMessagesRelatedWithAuthor(author)
    }
}