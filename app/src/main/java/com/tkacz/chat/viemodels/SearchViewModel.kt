package com.tkacz.chat.viemodels

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.tkacz.chat.view.states.SearchFieldState
import javax.inject.Inject

class SearchViewModel @Inject constructor() : ViewModel() {

    var searchFieldState = mutableStateOf(SearchFieldState.Closed)
    var searchText = mutableStateOf("")
}