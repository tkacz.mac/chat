package com.tkacz.chat.viemodels

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.tkacz.chat.data.IngredientsValidator
import com.tkacz.chat.view.states.InputTextState
import com.tkacz.chat.view.states.InputTextStateLogic
import javax.inject.Inject

class LoginViewModel @Inject constructor(private val ingredientsValidator: IngredientsValidator) :
    ViewModel(), InputTextStateLogic {

    var emailState = mutableStateOf(InputTextState.Idle)
    var passwordState = mutableStateOf(InputTextState.Idle)

    fun updateIngredientStates(email: String, password: String) {
        // email state
        processInputTextLogic(email, emailState, validator = {
            ingredientsValidator.validateEmail(it)
        })
        // password state
        processInputTextLogic(password, passwordState, validator = {
            ingredientsValidator.validatePassword(it)
        })
    }

    fun canMoveToAnotherActivity(): Boolean {
        return inputTextStatesAreCorrect(emailState.value, passwordState.value)
    }
}